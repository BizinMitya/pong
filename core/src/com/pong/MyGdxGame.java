package com.pong;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGdxGame extends Game {
    private static final int SCREEN_WIDTH = 800;
    private static final int SCREEN_HEIGHT = 600;
    private int speedPlayer = 0;
    private int speedPlayerPlatform = 3;
    private SpriteBatch batch;
    private Texture texturePlatform;
    private Texture textureBall;
    private Texture field;
    private Sprite platform;
    private Sprite bot;
    private Sprite ball;
    private int scorePlayer = 0;
    private int scoreBot = 0;
    private int speedX = 0;//скорость мяча по x
    private int speedY = 1;//скорость мяча по y
    private boolean flag = false;//false - к боту; true - к игроку
    private int speedBot = 0;
    private int speedXBot = 5;
    private int coordPlayer = 0;
    private int coordBot = 0;
    private BitmapFont font;
    private Menu menu;


    @Override
    public void create() {
        menu = new Menu();
        setScreen(menu);
        batch = new SpriteBatch();
        texturePlatform = new Texture("C:\\Users\\Dmitriy\\IdeaProjects\\Pong\\core\\assets\\platform.jpg");
        platform = new Sprite(texturePlatform);
        platform.setY(10);
        platform.setX(350);
        bot = new Sprite(texturePlatform);
        bot.setY(585);
        bot.setX(350);
        field = new Texture("C:\\Users\\Dmitriy\\IdeaProjects\\Pong\\core\\assets\\field.jpg");
        textureBall = new Texture("C:\\Users\\Dmitriy\\IdeaProjects\\Pong\\core\\assets\\ball.png");
        ball = new Sprite(textureBall);
        ball.setX(393);
        ball.setY(293);
        font = new BitmapFont();
    }

    @Override
    public void render() {
        //Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(field, 0, 0);
        ball.draw(batch);
        platform.draw(batch);
        bot.draw(batch);
        font.draw(batch, Integer.toString(scoreBot), 770, 320);
        font.draw(batch, Integer.toString(scorePlayer), 770, 290);
        //если мячик вылез за верх,то новый раунд(всё заново,по-умолчанию)
        if (ball.getY() + ball.getHeight() > bot.getY()) {
            ball.setX(393);
            ball.setY(293);
            platform.setX(350);
            platform.setY(10);
            bot.setX(350);
            bot.setY(585);
            scorePlayer++;
            //Main.textScorePlayer.setText(Integer.toString(Main.scorePlayer));
            speedY = 1;
            speedX = 0;
        }
        //если мячик вылез за низ,то новый раунд(всё заново,по-умолчанию)
        if (ball.getY() < platform.getY()) {
            ball.setX(393);
            ball.setY(293);
            platform.setX(350);
            platform.setY(10);
            bot.setX(350);
            bot.setY(585);
            scoreBot++;
            //Main.textScoreBot.setText(Integer.toString(Main.scoreBot));
            speedY = -1;
            speedX = 0;
        }
        //столкновение с ботом
        if (ball.getY() + ball.getHeight() >= bot.getY() &&
                ball.getX() <= bot.getX() + bot.getWidth() &&
                ball.getX() >= bot.getX()) {
            flag = false;
            speedBot = (int) (bot.getX() - coordBot);//Main.rectangle.getX() - coordBot>0 - смещение вправо
            if (speedBot != 0) {
                speedX += speedBot;
            }
            speedY = (-1) * speedY;
        } else {
            coordBot = (int) bot.getX();
        }
        //столкновение с игроком
        if (ball.getY() <= platform.getY() + platform.getHeight() &&
                ball.getX() <= platform.getX() + platform.getWidth() &&
                ball.getX() >= platform.getX()) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            flag = true;
            speedPlayer = (int) (platform.getX() - coordPlayer);
            if (speedPlayer != 0) {
                speedX += speedPlayer;
            }
            speedY = (-1) * speedY;
        } else {
            coordPlayer = (int) platform.getX();
        }
        //столкновение с левым бортом
        if (ball.getX() <= 0) {
            speedX *= (-1);
        }
        //столкновение с правым бортом
        if (ball.getX() + ball.getWidth() >= SCREEN_WIDTH - 2) {
            speedX *= (-1);
        }
        //мяч к игроку
        if (!flag) {
            ball.setY(ball.getY() + speedY);
            //проверяем, выходит ли мяч за левый борт
            if (ball.getX() + speedX >= 0) {
                if (ball.getX() + speedX + ball.getWidth() <= SCREEN_WIDTH - 2) {
                    ball.setX(ball.getX() + speedX);
                } else {
                    ball.setX(SCREEN_WIDTH - 2 - ball.getWidth());
                }
            } else {
                ball.setX(0);
            }
        }
        //мяч к боту
        if (flag) {
            ball.setY(ball.getY() + speedY);
            if (ball.getX() + speedX >= 0) {
                if (ball.getX() + speedX + ball.getWidth() <= SCREEN_WIDTH - 2) {
                    ball.setX(ball.getX() + speedX);
                } else {
                    ball.setX(SCREEN_WIDTH - 2 - ball.getWidth());
                }
            } else {
                ball.setX(0);
            }
        }
////////////////////Движение бота////////////////////////////////////////////
        if (bot.getX() <= 0) {
            speedXBot *= (-1);
        }
        if (bot.getX() + bot.getWidth() >= SCREEN_WIDTH) {
            speedXBot *= (-1);
        }
        if (ball.getX() <= bot.getX()) {
            if (speedXBot > 0) {
                speedXBot *= (-1);
            }
            if (bot.getX() + speedXBot >= 0) {
                bot.setX(bot.getX() + speedXBot);
            } else {
                bot.setX(0);
            }
        }
        if (ball.getX() + ball.getWidth() >= bot.getX() + bot.getWidth()) {
            if (speedXBot < 0) {
                speedXBot *= (-1);
            }
            if (bot.getX() + bot.getWidth() + speedXBot <= SCREEN_WIDTH) {
                bot.setX(bot.getX() + speedXBot);
            } else {
                bot.setX(SCREEN_WIDTH - bot.getWidth());
            }
        }
///////////////////////////////////////////////////////////////////////////////
        batch.end();
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if (platform.getX() + platform.getWidth() + speedPlayer <= SCREEN_WIDTH) {
                platform.setX(platform.getX() + speedPlayerPlatform);
            } else {
                platform.setX(SCREEN_WIDTH - platform.getWidth());
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (platform.getX() - speedPlayer >= 0) {
                platform.setX(platform.getX() - speedPlayerPlatform);
            } else {
                platform.setX(0);
            }
        }
    }

    @Override
    public void dispose() {
        font.dispose();
        batch.dispose();
    }
}
