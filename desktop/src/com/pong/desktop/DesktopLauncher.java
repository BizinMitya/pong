package com.pong.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.pong.MyGdxGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Пинг-понг";
        config.width = 800;
        config.height = 600;
        config.addIcon("core\\assets\\icon.png", Files.FileType.Internal);
        new LwjglApplication(new MyGdxGame(), config);
    }
}
